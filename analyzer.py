from __future__ import print_function
import pygal
import os

__author__ = 'miguegallard16@hotmail.com'

def fastaLoad(fl):
    fl = open(fl, 'r')
    fl = fl.readlines()
    list = {}
    tmp = ""
    seq = ""

    for idx, line in enumerate(fl):
        if '>' in line:
            list[tmp] = seq
            tmp = line.replace('\n', '').replace('>', '')
            seq = ""
            continue
        seq += line.replace('\n', '')
    return list
dic = fastaLoad('BB50012.tfa')

o = 0

for key in dic:
    if o == 0:
        o += 1
        continue

    f = open(key, 'w')
    print('Length of ' + key + ' ' + str(len(dic[key])))
    f.write(key + ':' + dic[key] + '\n')

from pyspark import SparkConf, SparkContext

o = 0

for key in dic:
    if o == 0:
        o += 1
        continue

    conf = SparkConf()
    cont = SparkContext(conf=conf)
    cont.setLogLevel("OFF")

    output = cont \
        .textFile(key) \
        .flatMap(lambda line: line.split(':')[1]) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .sortBy(lambda a: a[1], ascending=False) \
        .take(30)
    cont.stop()

    chart = pygal.Pie()

    chart.title = 'Number of aminoacids for ' + key

    for (word, count) in output:
        chart.add(word, int(count))
    chart.render_to_file(key + '.svg')

    os.remove(key)
